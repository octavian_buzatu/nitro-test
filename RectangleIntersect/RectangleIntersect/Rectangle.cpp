#include "stdafx.h"
#include <algorithm>
#include <iostream>
#include "Rectangle.h"

using namespace std;

Rectangle::Rectangle() : 
x(0), 
y(0), 
w(0), 
h(0)
{
}

Rectangle::Rectangle(int xInit, int yInit, int wInit, int hInit) :
x(xInit),
y(yInit), 
w(wInit), 
h(hInit)
{
}

void Rectangle::operator()(int xInit, int yInit, int wInit, int hInit)
{
    x = xInit;
    y = yInit;
    w = wInit;
    h = hInit;
}

bool Rectangle::readFromJson(rapidjson::Value& val)
{
	if (!val["x"].IsInt())
	{
		return false;
	}
	x = val["x"].GetInt();

	if (!val["y"].IsInt())
	{
		return false;
	}
	y = val["y"].GetInt();

	if (!val["w"].IsInt())
	{
		return false;
	}
	w = val["w"].GetInt();

	if (!val["h"].IsInt())
	{
		return false;
	}
	h = val["h"].GetInt();

	return true;
}

bool Rectangle::intersect(const Rectangle &r2, Rectangle &result) const
{
    result(0, 0, 0, 0);

	int left = max(x, r2.x);
	int top = max(y, r2.y);
	int right = min(x + w, r2.x + r2.w);
	int bottom = min(y + h, r2.y + r2.h);

	if (top < bottom && left < right)
	{
		result(left, top, right - left, bottom - top);
		return true;
	}

	return false;
} 

void Rectangle::display(const std::string& initialString) const
{
	cout << initialString.c_str() << " at (" << x << ", " << y << "), w = " << w << ", h = " << h << "." << endl;
}
