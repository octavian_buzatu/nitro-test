#ifndef RECT_H
#define RECT_H

#include "document.h"

struct Rectangle
{
    int x;
    int y;
    int w;
    int h;
    
    Rectangle();    
    Rectangle(int xInit, int yInit, int wInit, int hInit);
	bool readFromJson(rapidjson::Value& val);
    void operator()(int xInit, int yInit, int wInit, int hInit);
    bool intersect(const Rectangle &r2, Rectangle &result) const;
	void display(const std::string& initialString = _T("")) const;
};

#endif
