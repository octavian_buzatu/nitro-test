// RectangleIntersect.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <iostream>
#include "RectangleManager.h"

using namespace std;

int _tmain(int argc, _TCHAR* argv[])
{
	if (argc < 2)
	{
		cout << "Need to specify an argument for the JSON file containing the rectangle" << endl;
		return 1;
	}

	RectangleManager rectangleManager;
	if (!rectangleManager.readRectanglesFromJsonDoc(argv[1]))
	{
		cout << "JSON File containing the rectangles is missing or bad formatted" << endl;
		return 2;
	}
	rectangleManager.displayRectangles();

	rectangleManager.computeRectanglesIntersection();
	cout << endl;
	rectangleManager.displayRectanglesIntersection();

	return 0;
}

