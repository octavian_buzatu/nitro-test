#include "stdafx.h"
#include "RectangleManager.h"
#include "document.h"
#include "istreamwrapper.h"
#include <fstream>
#include <iostream>
#include <algorithm>

using namespace std;
using namespace rapidjson;


bool RectangleManager::readRectanglesFromJsonDoc(const std::string& jsonFile)
{
    ifstream ifstr(jsonFile);
    if (!ifstr)
    {
        return false;
    }
    
    IStreamWrapper istrWrapper(ifstr);
    
    Document jsonDoc;
    jsonDoc.ParseStream(istrWrapper);
    
    if (!jsonDoc.IsObject())
    {
        return false;
    }
    
    auto& rectsArray = jsonDoc["rects"];
    if (!rectsArray.IsArray())
    {
        return false;
    }
      
    for (auto iter = rectsArray.Begin(); iter != rectsArray.End(); ++iter)
    {
        if (!iter->IsObject())
        {
            return false;
        }

		Rectangle rectangle;
		if (!rectangle.readFromJson(*iter))
		{
			return false;
		}

		VectorIndexesType vectIndexes = { m_rectangleMap.size() };
		
		m_rectangleMap.insert(make_pair(vectIndexes, rectangle));
    }
    
    return true;
}

void RectangleManager::computeRectanglesIntersectionStep(const std::map<VectorIndexesType, Rectangle>& tempRectangleMap, std::map<VectorIndexesType, Rectangle>& stepRectangleIntersectMap) const
{
	stepRectangleIntersectMap.clear();

	for (auto iterTempRectangleMap = tempRectangleMap.begin(); iterTempRectangleMap != tempRectangleMap.end(); ++iterTempRectangleMap)
		for (auto iterRectangleMap = m_rectangleMap.begin(); iterRectangleMap != m_rectangleMap.end(); ++iterRectangleMap)
				{
					if (iterRectangleMap->first[0] <= iterTempRectangleMap->first[iterTempRectangleMap->first.size() - 1])
					{
						continue;
					}

					Rectangle rectIntersect;
					bool intersectResult = (iterTempRectangleMap->second).intersect(iterRectangleMap->second, rectIntersect);

					if (intersectResult)
					{
						auto indexesIntersect = iterTempRectangleMap->first;
						indexesIntersect.push_back(iterRectangleMap->first[0]);

						stepRectangleIntersectMap.insert(make_pair(indexesIntersect, rectIntersect));
					}
				}
}
    
void RectangleManager::computeRectanglesIntersection()
{
	m_rectangleIntersectMap.clear();

	auto maxStep = m_rectangleMap.size();
	auto tempRectangleMap = m_rectangleMap;

	for (auto step = 0; step < maxStep && !tempRectangleMap.empty(); ++step)
	{
		map<VectorIndexesType, Rectangle> stepRectangleIntersectMap;
		computeRectanglesIntersectionStep(tempRectangleMap, stepRectangleIntersectMap);
		
		m_rectangleIntersectMap.insert(stepRectangleIntersectMap.begin(), stepRectangleIntersectMap.end());

		tempRectangleMap = stepRectangleIntersectMap;
	}
}

void RectangleManager::displayRectangles() const
{
	cout << "Input:" << endl;
	for_each(m_rectangleMap.begin(), m_rectangleMap.end(), [](pair<VectorIndexesType, Rectangle> pairIndexRectangle)
	{
		pairIndexRectangle.second.display(string("\t") + to_string(pairIndexRectangle.first[0] + 1) + ": Rectangle");
	});
}

void RectangleManager::displayRectanglesIntersection() const
{
	if (m_rectangleIntersectMap.empty())
	{
		cout << "No rectangle intersection was found." << endl;
		return;
	}

	cout << "Intersections:" << endl;
	for_each(m_rectangleIntersectMap.begin(), m_rectangleIntersectMap.end(), [this](const pair<VectorIndexesType, Rectangle>& pairIndexesRectangle)
	{
		this->displayRectangleIntersection(pairIndexesRectangle);
	});
}

void RectangleManager::displayRectangleIntersection(const std::pair<VectorIndexesType, Rectangle>& pairIndexesRectangle) const
{
	auto vectorIndexes = pairIndexesRectangle.first;
	cout << "\tBetween rectangle ";

	for (int i = 0; i < vectorIndexes.size(); ++i)
	{
		cout << vectorIndexes[i] + 1;
		if (i < vectorIndexes.size() - 2)
		{
			cout << ", ";
		}
		else if (i == vectorIndexes.size() - 2)
		{
			cout << " and ";
		}
	}

	pairIndexesRectangle.second.display();
}


