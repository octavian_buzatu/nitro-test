#ifndef RECTANGLE_MANAGER
#define RECTANGLE_MANAGER

#include "Rectangle.h"
#include <map>
#include <string>
#include <vector>

using VectorIndexesType = std::vector<std::vector<int>::size_type>;

struct cmpByIndexes {
	bool operator()(const VectorIndexesType& indexes1, const VectorIndexesType& indexes2) const 
	{
		if (indexes1.size() < indexes2.size())
		{
			return true;
		}
		else if (indexes1.size() == indexes2.size())
		{
			auto iter1 = indexes1.begin();
			auto iter2 = indexes2.begin();
			for (; iter1 != indexes1.end() && iter2 != indexes2.end() && *iter1 == *iter2; ++iter1, ++iter2)
			{
			}

			if (*iter1 <= *iter2)
			{
				return true;
			}
		}

		return false;
	}
};

class RectangleManager
{
private:
	std::map<VectorIndexesType, Rectangle> m_rectangleMap;
	std::map<VectorIndexesType, Rectangle, cmpByIndexes> m_rectangleIntersectMap;
	void computeRectanglesIntersectionStep(const std::map<VectorIndexesType, Rectangle>& tempRectangleMap, std::map<VectorIndexesType, Rectangle>& stepRectangleIntersectMap) const;
	void displayRectangleIntersection(const std::pair<VectorIndexesType, Rectangle>& pairIndexesRectangle) const;

public:
    bool readRectanglesFromJsonDoc(const std::string& jsonFileContent);
	void computeRectanglesIntersection();
	void displayRectangles() const;
	void displayRectanglesIntersection() const;
};

#endif
